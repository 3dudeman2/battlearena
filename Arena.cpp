#include "Arena.h"

Arena::Arena() {
	int y = fillArena();
	if (y == -1) {
		cout << "The enemy data file could not be read" << endl;
		cout << "Please fix this and relaunch (:" << endl;
	}
	else {
	}
}
int Arena::fillArena() {
	ifstream infile1;
	infile1.open("Enemy.txt");

	if (!infile1.is_open())
	{
		cout << "Enemy.txt COULD NOT BE OPENED!!!" << endl;
		return -1;
	}
	string line1, dataString1;
	string substr1;
	Enemy* badGuy;
	while (getline(infile1, line1))
	{
		badGuy = new Enemy();
		istringstream dataStream(line1);
		getline(dataStream, substr1, '|'); //name
		badGuy->setName(substr1);
		getline(dataStream, substr1, '|');//damage
		badGuy->setDamage(stoi(substr1));
		getline(dataStream, substr1, '|'); //health
		badGuy->setHealth(stoi(substr1));
		Fighters.push_back(badGuy);
	}
	infile1.close();
	cout << "Enemies have been loaded!" << endl;
	return 0;
}
void Arena::printFighters() {
	for (size_t i = 0; i < Fighters.size(); i++) {
		cout << Fighters[i]->getName() << " | " << Fighters[i]->getDamage() << " damage" << endl;
	}
}
void Arena::dropLoot() {// determines if player is awarded loot or gold
	//roll to see if loot is awarded
}
Enemy* Arena::determineFighter(int x) {
	return Fighters[x-1];
}
void Arena::basicFight() {
	
}