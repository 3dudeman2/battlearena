#ifndef FANTASYSTORE_ARENA_H
#define FANTASYSTORE_ARENA_H
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include "Enemy.h"
using namespace std;

class Arena {
private:
	vector<Enemy*> Fighters;
	vector<Enemy*> Bosses;
public:
	Arena();
	int fillArena();
	void printFighters();
	void dropLoot();// determines if player is awarded loot or gold
	Enemy* determineFighter(int x);
	void basicFight();
	void bossFight();
};

#endif //!FANTASYSTORE_ARENA
