#ifndef FANTASYSTORE_ARMOUR
#define FANTASYSTORE_ARMOUR
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "SaleableItem.h"


class Armour : public SaleableItem {
	int defense;
public:
	Armour();
	int getDefense();
	void setDefense(int x);
};

#endif // !FANTASYSTORE_ARMOUR