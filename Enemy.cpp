#include "Enemy.h"

using namespace std;

Enemy::Enemy() {
	name = "bad guy";
	health = 100;
	maxHealth = 100;
	damage = 25;
}

void Enemy::oneShot() {
	health = 0;
}
int Enemy::getHealth() {
	return health;
}
void Enemy::setHealth(int x) {
	health = x;
	maxHealth = x;
}
int Enemy::getMaxHealth() {
	return maxHealth;
}
void Enemy::takeDamage(int x) {
	health -= x;
}
int Enemy::getDamage() {
	return damage;
}
string Enemy::getName() {
	return name;
}

void Enemy::fullHeath() {
	health = maxHealth;
}

void Enemy:: setDamage(int x){
	damage = x;
}
void Enemy:: setName(string x){
	name = x;
}
void Enemy::healthCheck() {
	if (getHealth() > 0)
		cout << getName() << ": " << getHealth() << " HP/ " << getMaxHealth() << " HP" << endl << endl;
	if (getHealth() <= 0)
		cout << getName() << " has 0 health remaining!" << endl;
}
void Enemy::statAdjust(int x) {
	health = health * x;
	maxHealth = maxHealth * x;
	damage = damage * x;
}

void Enemy::statUndo(int x) {
	health = health / x;
	maxHealth = maxHealth/ x;
	damage = damage / x;
}

void Enemy::resetEnemy(int x) {
	statUndo(x);
	fullHeath();
}

string Enemy::namePrefix(int x) {
	switch (x) {
	case 1: {
		return "Annoyed ";
		break;
	}
	case 2: {
		return "Angry ";
		break;
	}
	case 3: {
		return "Fuming ";
		break;
	}
	case 4: {
		return "Enraged ";
		break;
	}
	case 5: {
		return "Nightmare ";
		break;
	}
	}
}