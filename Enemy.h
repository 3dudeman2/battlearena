#ifndef FANTASYSTORE_ENEMY_H
#define FANTASYSTORE_ENEMY_H
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

class Enemy{
private:
	int maxHealth, health, damage;
	string name;
public:
	Enemy();
	void oneShot();
	void fullHeath();
	void setHealth(int x);
	void setDamage(int x);
	void takeDamage(int x);
	void setName(string x);
	string getName();
	int getMaxHealth();
	int getHealth();
	int getDamage();
	void healthCheck();
	void statAdjust(int x);
	void statUndo(int x);
	void resetEnemy(int x);
	string namePrefix(int x);
};

#endif //!FANTASYSTORE_ENEMY