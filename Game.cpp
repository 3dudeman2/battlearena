/*
Author: Frank Leyva
File Name: FantasyStore.cpp
Date: 11/13/16
Assignment Summary:
For this project, we are to build 
a functioning store that allows a 
player to buy items. It will be 
used in the upcoming Fantasy Arena project as well

This file contains:
loadGame();
saveGame();
equipItem();
purchaseItems();
storeMenu();
arenaFights()
arenaMenu();
debugMenu();
main();

TODO: fix the enemy() issuse and add grunt(), add in specialItems, add boss fight, add in training area in arena, add in Intro/tutorial, add in debug menu!!!
KNOWN BUGS:

*/

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Game.h"

using namespace std;

///////////LOAD GAME//////////////
//This function reads the Save1.txt file and 
//sets all the values for the player accordingly
void loadGame(Player *user, Shop *shop) {
    ifstream saveFile;
    saveFile.open("Save1.txt");

    if (!saveFile.is_open())
    {
        cout << "Save1.txt could not be opened" << endl;
    }
    string line1, dataString1, items, item;
    string substr1;
    while (getline(saveFile, line1))
    {
        istringstream dataStream(line1);
        getline(dataStream, substr1, '|'); //name
        user->setName(substr1);
		getline(dataStream, substr1, '|');//max health
		user->setMaxHealth(stoi(substr1));
		getline(dataStream, substr1, '|');//health
		user->setHealth(stoi(substr1));
		getline(dataStream, substr1, '|');//attack
		user->setPAttack(stoi(substr1));
		getline(dataStream, substr1, '|');//defense
		user->setPDefense(stoi(substr1));
        getline(dataStream, substr1, '|');//gold
        user->setGold(stoi(substr1));
        getline(dataStream, substr1, '|');//level
        user->setLevel(stoi(substr1));
		getline(dataStream, substr1, '|');//xp
		user->setXp(stoi(substr1));
		getline(dataStream, substr1, '|');//ID ofEquipped weapon
		if (stoi(substr1) == -1)
			cout << "No weapon equipped!" << endl;
		else {
			cout << substr1 << endl;
			user->equipItem(shop->idChecker(stoi(substr1)));
		}
		getline(dataStream, substr1, '|');//ID ofEquipped armour
		if (stoi(substr1) == -1)
			cout << "No armour equipped" << endl;
		else
			user->equipItem(shop->idChecker(stoi(substr1)));
        getline(dataStream, items, '|');//ids of items in bag
        istringstream itemStream(items);
        while (getline(itemStream, item, ',')) {
			if (stoi(item) == -1)
				cout << "Your bag is empty!" << endl;
			else
            user->bag.getItem(shop->idChecker(stoi(item)));//adds the items to the players bag
        }
        getline(dataStream, substr1, '|'); //rank
        user->setRank(stoi(substr1));
        getline(dataStream, substr1, '|');//division
        user->setDivision(stoi(substr1));
        cout << "THE GAME HAS BEEN SUCCESSFULLY LOADED!" << endl << endl;
    }
    
    saveFile.close();
}
//////////SAVE GAME//////////
//This functions gets all the values of the player data members
//then writes them to the Save1.txt file in the correct order
void saveGame(Player *user) {
    string x;
    ofstream saveFile("Save1.txt");
    if (saveFile.is_open()) {
        saveFile << user->getName() << "|"; //name
		saveFile << user->getMaxHealth() << "|";//max health
		saveFile << user->getHealth() << "|";//health
		saveFile << user->getPAttack() << "|"; //attack
		saveFile << user->getPDefense() << "|"; //defense
        saveFile << user->getGold() << "|";//gold
        saveFile << user->getLevel() << "|";//level
		saveFile << user->getXp() << "|";//xp
		saveFile << user->getEquippedWeaponID() << "|";//ID of equipped weapon
		saveFile << user->getEquippedArmourID() << "|";//ID of equipped armour
		if (user->bag.getSize() > 0) {
			for (int i = 0; i < user->bag.getSize(); i++) {//IDs of items in bag separated by , and ending with |
				saveFile << user->bag[i]->getID() << ",";
			}
		}
		else
			saveFile << "-1,";
        saveFile << "|";
        saveFile << user->getRank() << "|";//rank
        saveFile << user->getDivision() << "|";//division
        cout << "THE GAME HAS BEEN SUCCESSFULLY SAVED!" << endl << endl;
        saveFile.close();
    }
    else
        cout << "Could not save to file! " << endl;

}

////////////////EQUIP ITEM//////////
//This function allows the player to equip their items
//This functions deals with armour and weapons since the equipping of 
//both is pretty similar.
void equipItem(Player *user) {
	bool hasWeap=false;
	bool hasArm=false;
    int answer=3;
    int weap;
    int arm;
    do {
		if (user->bag.getSize() == 0)
			cout << endl << "Your bag is empty!" << endl << endl;
		else {
			for (int i = 0; i < user->bag.getSize(); i++) {
				if (user->bag[i]->getType() == 'w')
					hasWeap = true;
				else;
			}
			for (int i = 0; i < user->bag.getSize(); i++) {
				if (user->bag[i]->getType() == 'a')
					hasArm = true;
				else;
			}
		}
        cout << "//////////////////////////////////////" << endl;
        cout << "/  What would you like to equip?     /" << endl;
        cout << "/ [0] Weapon                         /" << endl;
        cout << "/ [1] Armor                          /" << endl;
        cout << "/ [2] Return to Main Menu            /" << endl;
        cout << "//////////////////////////////////////" << endl;
        cin >> answer;
		if (answer == 0 && hasWeap == true) {//weapon
			user->bag.printWeapons();
			//show equiped weapon
			cout << "Which Weapon do you want to use?" << endl;
			cin >> weap;
			while (weap < 0 || weap >= user->bag.getSize() || user->bag[weap]->getType() != 'w') {
				cout << "I didnt catch that!" << endl;
				cout << "Which weapon do you want to use?" << endl;
				cin >> weap;
			}
			user->equipItem(user->bag.get(weap));
			user->printEquippedWeapon();
			user->bag.removeItem(weap);
			hasWeap = false;
			answer = 3;
		}
		else if (answer==0 && hasWeap==false) {
			cout << "You arent carrying any weapons!" << endl;
			answer = 3;
		}
		if (answer == 1 && hasArm==true) {//armour
			user->bag.printArmour();
			//show equipped armour?
			cout << "Which Armour do you want to use?" << endl;
			cin >> arm;
			while (arm < 0 || arm >= user->bag.getSize() || user->bag[arm]->getType() != 'a') {
				cout << "I didnt catch that!" << endl;
				cout << "Which armour do you want to use?" << endl;
				cin >> weap;
			}
			user->equipItem(user->bag.get(arm));
			user->printEquippedArmour();
			user->bag.removeItem(arm);
			answer = 3;
			hasArm = false;
        }
		else if (answer==1 && hasArm==false){
			cout << "You arent carrying any armour!" << endl;
			answer = 3;
		}
    } while (answer != 2);
}

////////////////////PURCHASE ITEMS////////////////////////////////////
//This function allows the player to purchase their items.
//It totals up the gold cost of the cart and checks if the player as enough to buy them.
//It then just adds all the items in the cart into the players bag allowing them to use them.
void purchaseItems(Shop *shop, Player *user) {
    int cost;
    cost = shop->checkOut();
    if (cost == 0) {

    }
    else if (cost <= user->getGold() && cost != 0) {
        int newGold = user->spendGold(cost);
        user->setGold(newGold);
        for (size_t i=0; i < shop->cart.size(); i++) {
            SaleableItem *u;
            u=shop->sendCartItem(i);
            user->bag.getItem(u);
        }
            shop->clearCart();
            user->bag.printBag();

    }
    else {
        user->noGold();
            cout << "ShopKeeper: Oh...It looks like you dont have enough money..." << endl;
            cout << "ShopKeeper: Maybe you should add less things to your cart!" << endl;
            shop->askToRemove();
    }
}
////////////////////STORE MENU////////////////////////////////////
//This function displays the menu for the shop.
//It allows the player to choose Leave, Add To Cart, Remove From Cart, or Sell.
void storeMenu(Shop *shop, Player *user) {
    string b;
    int newMoney;
    int cartCheck = 1;
    int bagCheck = 0;
	user->storeIntro();
    cout << "ShopKeeper: Welcome to my Shop!" << endl;
    do {
        
        if (shop->cart.empty() == 0) {
            shop->printCart();
        }
        cout << "//////////MANDALEC'S SHOP/////////////" << endl; 
        cout << "/ What would you like to do?         /" << endl;
        cout << "/ [1] Leave Shop                     /" << endl;
        cout << "/ [2] Add item to cart               /" << endl;
        if (shop->cart.empty() == 0) {
            cout << "/ [3] Remove Item from cart          /" << endl;
            cout << "/ [4] Checkout                       /" << endl;
            cartCheck = 2;
        }
        if ( user->bag.getSize() > 0) {
        cout << "/ [5] Sell Item                      /" << endl;
            bagCheck= 2;
        }
        cout << "//////////////////////////////////////" << endl;
        cout << "Please enter the number for the action you would like." << endl;
        cin >> b;
        cout << endl;
        if (b == "2") {
            user->goldCheck();
            shop->addItemsToCart();
        }
        if (b == "3" && cartCheck!=1) {
            shop->askToRemove();
            cartCheck = 1;
        }
        if (b == "4" && cartCheck!=1) {
            purchaseItems(shop, user);
            cartCheck = 1;
            }
        if (b == "5"&& bagCheck>1) {
            newMoney=user->bag.sellItems();
            user->earnGold(newMoney);
            user->goldCheck();
            cartCheck = 1;
            bagCheck = 1;
        }
        } while (b != "1");
}
////////////////////FIGHTING ARENA////////////////////////////////////
//This function deals with all the logic of the fights.
//It allows the player to Attack, use an Item, or run.
//After the players turn, the monster gets to attack
//Depending on who wins the battle, it displays a message 
int arenaFight(Arena *arena, Player *user) {
	user->arenaIntro();
    int choice;
    int division = user->getDivision();
    Enemy* monster = arena->determineFighter(division);
    monster->statAdjust(user->getRank());
	string monsterName = monster->namePrefix(user->getRank()) + monster->getName();
    cout << "LADIES AND GENTLEMEN! In today's fight, we bring you..." << endl;
    cout << user->getName() << " VS " << monsterName << endl << endl;
    cout << "Let the fight......begin!" << endl << endl;
    user->healthCheck();
	cout << monsterName;
    monster->healthCheck();
    do {
			cout << "It is your turn! What would you like to do?" << endl;
			cout << "//////////////////////////////////////" << endl;
			cout << "/ [1] Attack                         /" << endl;
			cout << "/ [2] Item                           /" << endl;
			cout << "/ [3] Give Up                        /" << endl;
			cout << "//////////////////////////////////////" << endl;
			cin >> choice;
			if (choice == 1) {
				cout << endl;
				int pDamage = user->attack(); //returns damage number
				if (pDamage == 0) {
					cout << "Your attack missed!" << endl;
				}
				else {
					cout << "You attack for " << pDamage << " damage." << endl << endl;
				}
				monster->takeDamage(pDamage);
				cout << monsterName;
				monster->healthCheck();
			}
			if (choice == 2) {
				int potion;
				user->bag.printItems();
				cout << "Which item do you want to use?" << endl;
				cin >> potion;
				while (potion < 0 || potion >= user->bag.getSize() || user->bag[potion]->getType() != 'i') {
					cout << "I didnt catch that!" << endl;
					cout << "Which item do you want to use?" << endl;
					cin >> potion;
				}
				user->heal(user->bag[potion]->getExtraHealth());// heals the player!
				//use item
			}
			if (choice == 3) {
				cout << "You ran!" << endl;
				//monster->oneShot();
				//user flees from battle
			}
        if (monster->getHealth() > 0 && (choice==1 || choice==2)) {
            Sleep(1000);
            cout << "It is the " << monsterName << "'s turn!" << endl << endl;
			cout << "The " << monsterName << " attacks you for " << monster->getDamage() << " damage!" << endl;
            int outDamage = monster->getDamage();
			user->takeDamage(outDamage);
            user->healthCheck();
            cout << endl;
        }
	} while (choice!=3 && (user->getHealth() > 0 && monster->getHealth() > 0));
	monster->resetEnemy(user->getRank());
	if (choice == 3) {
		cout << "You surrdender to your foe" << endl;
		//Test for gold loss or some shit
		return 0;
	}
	else {
		if (user->getHealth() <= 0) {//player died
			user->playerRevive();
			return 0;
		}
		else {//drops loot or gold   // player won
			user->addXp();
			user->playerVictory();
			cout << "Rixo: Great fight kid!" << endl;
			return 1;
		}
	}
}
////////////////////ARENA MENU////////////////////////////////////
//This function displays the menu for the arena.
//It allows the player to choose Fight, Training, Leave.
void arenaMenu(Arena *arena, Player *user) {
    int x = 0;
    do {
        if (x == 0) {
            cout << "///////////////ARENA MENU/////////////" << endl;
            cout << "/ What would you like to do?         /" << endl;
            cout << "/ [1] Fight                          /" << endl;
            cout << "/ [2] Leave                          /" << endl;
            cout << "//////////////////////////////////////" << endl;
            cin >> x;
            cout << endl;
            switch (x) {
            default: {
                cin.clear();
                cin.ignore();
                break;
            }
            case 1: {
                int y = arenaFight(arena, user);
                if (y == 0) { //player died or fled
					//idk?
                }
                if (y == 1) {// player won
                    //gets gold or item!
                }
				x = 0;
				break;
            }
            case 2: {
                x = 2;
				break;
                }
            }
       }
    } while (x!=2);
    cout << "Thanks for coming!" << endl << endl;
}
////////DEBUG MENU////
//this allows a player to determine their starting stats
//perfect for teachers who are wanting to test out the epic boss fights
void debugMenu(Player *user) {
	int choice;
	int play = 0;
	cout << "Welcome to the debug menu!" << endl;
	cout << "Here you can change all your heros stats!" << endl;
	cout << "/////////OPTIONS!//////////" << endl;
	cout << "/ What would you like to do?         /" << endl;
	cout << "/ [1] Name                           /" << endl;
	cout << "/ [2] Max Health                     /" << endl;
	cout << "/ [3] Gold                           /" << endl;
	cout << "/ [4] Rank                           /" << endl;
	cout << "/ [5] Division                       /" << endl;
	cout << "/ [6] Play!                          /" << endl;
	cout << "//////////////////////////////////////" << endl;
	cin >> choice;
	while (play != 1) {
		switch (choice) {
		default: {
			cin.clear();
			cin.ignore();
			break; }
		case 1: {
			cout << "Please enter your name!" << endl;
			string newName;
			cin >> newName;
			user->setName(newName);
			break;
		}
		case 2: {
			cout << "Please enter the max health!" << endl;
			int newHealth;
			cin >> newHealth;
			user->setMaxHealth(newHealth);
			user->setHealth(newHealth);
			break;
		}
		case 3: {
			cout << "Please enter the amount of gold you want!" << endl;
			int newGold;
			cin >> newGold;
			user->setGold(newGold);
			break;
		}
		case 4: {
			cout << "Please enter your Rank (1-5)" << endl;
			int newRank;
			cin >> newRank;
			user->setRank(newRank);
			break;
		}
		case 5: {
			cout << "Please enter your Division (1-5)" << endl;
			int newDivision;
			cin >> newDivision;
			user->setDivision(newDivision);
			break;
		}
		case 6: { 
			cout << "Lets do this!" << endl;
			play = 1;
			break;
		}
		}
	}
}

///////////////MAIN FUNCTION//////////
//Runs the game//
int main() {
    Arena *arena = new Arena();
    Shop *shop=new Shop();
    Player *user=new Player();
    int x = 0;
    int loadOrNah;
    int breakLoop=0;
    while (breakLoop!=1) {
        cout << "**********Welcome to my game!************" << endl;
        cout << "* [1] New game                          *" << endl;
        cout << "* [2] Load game                         *" << endl;
        cout << "*****************************************" << endl;
        cin >> loadOrNah;
        switch (loadOrNah) {
        default: {
            cin.clear();
            cin.ignore();
            break; }
        case 1: {
            user->newGame();
            breakLoop = 1;
            break; }
        case 2: {
            loadGame(user, shop);
            breakLoop = 1;
            break; }
		case 3: {
			debugMenu(user);
			breakLoop = 1;
			break; }
        }
    }
    do {
        if (x == 0) {
			user->printHub();
            cin >> x;
            cout << endl;
            switch (x) {
            default: {
                cin.clear();
                cin.ignore();
                break; }
            case 1: {
                arenaMenu(arena, user);
                x = 0;
                break; }
            case 2: {
                storeMenu(shop, user);
                x = 0;
                break; }
            case 3: {
                equipItem(user);
                x = 0;
                break; }
            case 4: {
                user->printStats();
                x = 0;
                break; }
            case 5: {
                saveGame(user);
                x = 0;
                break; }
            case 6: {
                break; }
            }
        }
        if (x > 6 || x < 0) {
            cout << "*You hear a voice in the distance*" << endl;
            cout << "???: I did not quite catch that" << endl;
            x = 0;
        }
    }
    while (x != 6);
    cout << "Thanks for playing! I sure hope you saved....." << endl;
}