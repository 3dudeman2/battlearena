#ifndef FANTASYSTORE_INVENTORY_H
#define FANTASYSTORE_INVENTORY_H
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "SaleableItem.h"
#include "Weapon.h"
#include "Armour.h"
#include "Item.h"

using namespace std;

class Inventory  {
public:
	vector<SaleableItem*> bag;
	Inventory();
	SaleableItem* operator[](int x);
	void printWeapons();
	void printArmour();
	void printItems();
	void printBag();
	int getSize();
	int sellItems();
	void getItem(SaleableItem *x);
	void removeItem(int x);
	int searchBag(string item);
	int itemSold(int k);
	SaleableItem* get(int x) {
		SaleableItem* tmp = bag[x];
		return tmp;
	}
};
#endif // !FANTASYSTORE_INVENTORY_H