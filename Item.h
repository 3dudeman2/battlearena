#ifndef FANTASYSTORE_ITEM
#define FANTASYSTORE_ITEM
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "SaleableItem.h"

class Item : public SaleableItem {
	int extraHealth;
public:
	Item();
	void setExtraHealth(int x);
	int getExtraHealth();
};

#endif // !FANTASYSTORE_ITEM