#include "Player.h"

Player::Player() {
	name = "Luke";
	maxHealth = 100;
	health = 100;
	mana = 100;
	gold = 0;
	level = 1;
	xp = 0;
	damage = 10;
	armour = 0;
	rank = 1;
	division = 1;
	equippedWeapon = NULL;
	equippedArmour = NULL;
}
void Player::newGame() {
	intro = true;
	arenaTutorial = true;
	storeTutorial = true;
	cout << "Please input your name!" << endl;
	cin.ignore();
	getline(cin, name);
	cout << endl;//ADD IN INTO TO GAME
}
void Player::setHealth(int x) {
	health = x;
}

void Player::setMaxHealth(int x) {
	maxHealth = x;
}


int Player::getPAttack(){
	return damage;
}
int Player::getPDefense(){
	return armour;
}
void Player::setPAttack(int x){
	damage = x;
}
void Player::setPDefense(int x){
	armour = x;
}

int Player::getEquippedWeaponID() {
	if (equippedWeapon == NULL)
		return -1;
	else
		return equippedWeapon->getID();
}

int Player::getEquippedArmourID() {
	if (equippedArmour == NULL)
		return -1;
	else
		return equippedArmour->getID();
}
void Player::arenaIntro() {
	if (arenaTutorial == true){
		cout << endl;//ADD IN INTRO TO ARENA
		}
	arenaTutorial = false;
		
}
void Player::storeIntro() {
	if (storeTutorial == true) {
		//ADD IN INTO TO STORE
	}
	arenaTutorial = false;
}

void Player::setRank(int x) {
	if (x > 0 && x <= 5)
		rank = x;
	else
		cout << "The rank you are trying to set will not work!" << endl;
}
void Player::setDivision(int x) {
	if (x > 0 && x <= 5)
		division = x;
	else
		cout << "The division you are trying to set will not work!" << endl;
}
void Player::setLevel(int x) {
	level = x;
}
void Player::setGold(int x) {
	gold = x;
}
int Player::getMaxHealth() {
	return maxHealth;
}
void Player::setXp(int x) {
	xp = x;
}
int Player::getLevel() {
	return level;
}

void Player:: skillPoints() {
	int answer;
	int done=0;
	cout << endl;
	cout << "LEVEL UP!" << endl;
	cout << "*You are now level " << getLevel() << "!" << endl;
	do {
		cout << "//////////////////////////////////////" << endl;
		cout << "/ Which upgrade would you like?      /" << endl;
		cout << "/[1] 5 points into Damage            /" << endl;
		cout << "/[2] 5 points into Armor             /" << endl;
		cout << "/[3] 10 points into Health           /" << endl;
		cout << "//////////////////////////////////////" << endl;
		cin >> answer;
		if (answer == 1) {
			damage += 5;
			cout << "5 points have been added to Damage!" << endl;
			done = 1;
		}
		else if (answer == 2) {
			armour += 5;
			cout << "5 points have been added to Armor!" << endl;
			done = 1;
		}
		else if (answer == 3) {
			maxHealth += 10;
			cout << "10 points have been added to health!" << endl;
			done = 1;
		}
		else
			cout << "Try again!" << endl;
	} while (done==0);
}

void Player::levelUpCheck() {
	if (xp >= 100) {
		level += 1;
		xp = 0;
		skillPoints();
	}
}
int Player::getXp() {
	return xp;
}

void Player::addXp() { //just adds 25 xp, subject to change
	xp += 25;
	levelUpCheck();
}

string Player::getName() {
	return name;
}
void Player::setName(string x) {
	name = x;
}
int Player::getHealth() {
	return health;
}

void Player::takeDamage(int incDamage) {
	if (equippedArmour == NULL) {
		cout << "You blocked " << armour << " damage!" << endl << endl;
		health = health - (incDamage - armour);
	}
	else {
		int totalArmour = armour + equippedArmour->getDefense();
		int damageTaken = incDamage - totalArmour;
		int blocked = totalArmour;
		if (damageTaken < 0) {
			damageTaken = 0;
			blocked = incDamage;
		}
		cout << "You blocked " << blocked << " damage." << endl << endl;
		cout << "You took " << damageTaken << " damage!" << endl;
		health -= damageTaken;
	}

}
int Player::getGold() {
	return gold;
}
void Player::recieveGold() {
	int roll = diceRoll();
	int reward;
	if (roll >= 11)
		reward = 5;
	else
		reward = 4;
	cout << "You recieved " << reward << " gold!" << endl;
	gold += reward;
	cout << "You now have " << gold << " gold." << endl << endl;
}
void Player::goldCheck() {
	cout << "*You are carrying " << getGold() << " gold*" << endl;
}
void Player::noGold() {
	cout << "*You realize that you dont have enough gold*" << endl;
}
int Player::spendGold(int x) { // spends "x"
	gold = getGold() - x;
	cout << "*You pay the " << x << " gold, leaving you with " << getGold() << " gold left*" << endl <<endl;
	return gold;
}
void Player::increaseHealth(int x) {
	maxHealth += x;
}
void Player::increaseDamage(int x) {
	damage += x;
}
void Player::earnGold(int x) {
	gold = gold + x;
}
int Player::getRank() {
	return rank;
}
int Player::getDivision() {
	return division;
}

void Player::divisionUp() { //called after a battle is won
	division += 1;
}
int Player::checkForRankUp() {//Called after battle, determines if the player is ready to rank up
	if (division <= 5)
		return 0;
	else
		return 1;
}
int Player::rankUp(int x) {//takes the number from divisionCheck() then returns 1 for rank up, 2 for max rank, and 0 if didnt rank up
	if (x == 1 && rank!=6) {
		rank += 1;
		division = 1;
		return 1;
	}
	if (x==1 && rank == 6) {
		return 2;
	}
	else
		return 0;
}

void Player::playerVictory() {//called after player beats an enemy
	recieveGold();
	divisionUp();
	int check = checkForRankUp();
	int maxRankCheck=rankUp(check);
	cout << "You are now Rank: " << rank << "| Division: " << division << endl;
	if (maxRankCheck == 2) {
		cout << "You are now the max rank!" << endl;
		//return
	}
	if (maxRankCheck == 1) {
		cout << "Rixo: Congratz on beating that boss and ranking up!" << endl;
		cout << "Rixo: These next set of battles are going to be a bit harder!" << endl;
		//return
	}
	if (maxRankCheck == 0) {
		cout << "Rixo: Let me heal you up!" << endl;
		fullHeal();
		//return
	}
}
void Player::equipItem(SaleableItem* x) {
	char type = x->getType();
	switch (type) {
	default: {cout << "You cannot equipped that here!" << endl; 
		break; }
	case 'w': {
		if (equippedWeapon == NULL) {
			equippedWeapon = x;
		}
		else {
			SaleableItem* tmp = equippedWeapon;
			equippedWeapon = x;
			bag.getItem(tmp);
		}
		break; }
	case 'a': {
		if (equippedArmour == NULL) {
			equippedArmour = x;
		}
		else {
			SaleableItem* tmp = equippedArmour;
			equippedArmour = x;
			bag.getItem(tmp);
		}
		break; }		
	}
}


int Player:: attack() {//returns the damage of the player
	int roll = diceRoll();
	if (equippedWeapon == NULL) {
		if (roll >= 2) {
			return damage;
		}
		else
			return 0;
	}
	else {/////////easier way to do this maybe????//////////////////////////////
        if (roll <= 1) {
            return 0;
        }
        else if (roll > 1 && roll < 9) {
            return damage + equippedWeapon->getDamage()-2;
        }
		else if (roll >=9 && roll <16) {
			return damage + equippedWeapon->getDamage();
		}
		else if (roll >=16 && roll <= 19) {
			return damage + equippedWeapon->getDamage() + 2;
		}
        else
            cout << "Critical Hit!" << endl;
			return (damage + equippedWeapon->getDamage()) * 2;
	}
}
void Player::heal(int x) {
	if (x + health >= maxHealth) {
		fullHeal();
	}
	else {
		health += x;
		cout << "You were healed for " << x << " health!" << endl;
	}
}

void Player::healthCheck() {
	if(getHealth() > 0)
		cout << getName() << ": "<< getHealth() << " HP/ " << getMaxHealth() <<" HP" <<endl;
	if (getHealth() <= 0) 
		cout << getName() << " has 0 health remaining!" << endl;
}

void Player::playerRevive() {
	cout << endl;
	cout << "*You open your eyes and see a mysterious, angel like figure in front of you*" << endl << endl;
	cout << "???: Hi! It looks like you are dead!" << endl;
	cout << "???: My name is Faith and I am here to revive you!" << endl;
	health = maxHealth;
	cout << "Faith: There we go! Looking better already!" << endl;
	cout << "Faith: Try to be more careful next time!" << endl << endl;
}
int Player::diceRoll() {//rolls a dice 1-20
	int roll;
	srand(time(NULL));
	roll = 1 + rand() % 20;
	return roll;
}
void Player::fullHeal() {
	health = maxHealth;
	cout << "You have been fully healed!" << endl;
}

void Player::printEquippedWeapon() { //need to just copy and switch name for armour!
	if (equippedWeapon != NULL) {
		cout << "You are using ";
		if (equippedWeapon->getName()[0] == 'a' || // make vowelCheck();
			equippedWeapon->getName()[0] == 'e' ||
			equippedWeapon->getName()[0] == 'i' ||
			equippedWeapon->getName()[0] == 'o' ||
			equippedWeapon->getName()[0] == 'u') {
			cout << "an" << equippedWeapon->getName() << " that does " << equippedWeapon->getDamage() << " damage." << endl;
		}
		else {
			cout << "a " << equippedWeapon->getName() << " that does " << equippedWeapon->getDamage() << " damage." << endl;
		}
	}
	else
		cout << "You do not have an item equipped in this slot!" << endl;
}
void Player::printEquippedArmour() {
	if (equippedArmour != NULL) {
		cout << "You are wearing ";
		if (equippedArmour->getName()[0] == 'a' || // make vowelCheck()?;
			equippedArmour->getName()[0] == 'e' ||
			equippedArmour->getName()[0] == 'i' ||
			equippedArmour->getName()[0] == 'o' ||
			equippedArmour->getName()[0] == 'u') {
			cout << "an" << equippedArmour->getName() << " that has " << equippedArmour->getDefense() << " armour." << endl;
		}
		else {
			cout << "a " << equippedArmour->getName() << " that has " << equippedArmour->getDefense() << " armour." << endl;
		}
	}
	else
		cout << "You do not have an item equipped in this slot!" << endl;
}

void Player::printStats() {
	cout << "******************STATS*******************" << endl;
	cout << "*Name: " << getName() << endl; //name
	cout << "*" << getHealth() << " HP/ " << getMaxHealth() << " HP" << endl;//health / max health
	cout << "*Level: " << getLevel() << endl;//level
	cout << "*" << getXp() << "XP/100XP" << endl;//xp / 100
	cout << "*Gold: " << getGold() << endl;//gold
	cout << "*Attack: " << damage << endl;//attack
	cout << "*Defense: " << armour << endl;//defense
	cout << "*Weapon: ";
	printEquippedWeapon();//weapon
	cout << "*Armour: ";
	printEquippedArmour();//armour 
	cout << "*You are Rank: " << rank << "| Division: " << division << endl;//rank | divsion
	cout << "******************************************" << endl;
}

void Player::printHub() {
	cout << "/////////WELCOME TO THE HUB!//////////" << endl;
	cout << "/ What would you like to do?         /" << endl;
	cout << "/ [1] Go To Arena                    /" << endl;
	cout << "/ [2] Head to Shop                   /" << endl;
	cout << "/ [3] Equip Item                     /" << endl;
	cout << "/ [4] Stats and Inventory            /" << endl;
	cout << "/ [5] Save                           /" << endl;
	cout << "/ [6] Quit                           /" << endl;
	cout << "//////////////////////////////////////" << endl;
}