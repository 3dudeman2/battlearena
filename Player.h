#ifndef FANTASYSTORE_PLAYER_H
#define FANTASYSTORE_PLAYER_H


#include "Inventory.h"
#include <fstream>
#include <sstream>
#include <time.h>

using namespace std;

class Player {
protected:
	bool intro;
	bool storeTutorial;
	bool arenaTutorial;
	string name;
	int maxHealth;
	int health;
	int mana;
	int gold;
	int level;
	int xp;
	int armour;
	int damage;
	int rank; //1-5
	int division; // 1-5, after 5, resets to 1
	SaleableItem *equippedWeapon;
	SaleableItem *equippedArmour;

public:	Player();
		void arenaIntro();
		void storeIntro();
		void newGame();
		Inventory bag;
		void setHealth(int x);
		void setMaxHealth(int x);
		int getEquippedWeaponID();
		int getEquippedArmourID();
		int getPAttack();
		int getPDefense();
		void setPAttack(int x);
		void setPDefense(int x);
		void setRank(int x);
		void setDivision(int x);
		void setName(string x);
		void setGold(int x);
		void setLevel(int x);
		void setXp(int x);
		int getLevel();
		void skillPoints();
		void levelUpCheck();
		int getXp();
		void addXp();
		string getName();
		int getMaxHealth();
		int getHealth();
		void takeDamage(int x);
		int getGold();
		void recieveGold();
		void goldCheck();
		void noGold();
		int spendGold(int x);
		void increaseHealth(int x);
		void increaseDamage(int x);
		void earnGold(int x);
		int getRank();
		int getDivision();
		void equipItem(SaleableItem* x);
		int attack();
		void divisionUp();
		int checkForRankUp();
		int rankUp(int x);
		void heal(int x);
		void healthCheck();
		void playerVictory();
		void playerRevive();
		int diceRoll();
		void fullHeal();
		void printEquippedWeapon();
		void printEquippedArmour();
		void printStats();
		void printHub();
};

#endif // !FANTASYSTORE_PLAYER_H