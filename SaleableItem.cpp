#include "SaleableItem.h"

SaleableItem:: SaleableItem() {
	name = "name";
	value = 0;
	ID = 0;
	stats = "stat";
}
void SaleableItem:: setName(string name) {
	this->name = name;
}
void SaleableItem::setValue(int value) {
	this->value = value;
}
void SaleableItem::setID(int id) {
	this->ID = id;
}
void SaleableItem::setStats(string stat){
	this->stats = stat;
}
void SaleableItem::setType(char x) {
	this->type = x;
}
char SaleableItem::getType() {
	return type;
}
string SaleableItem::getName() {
	return name;
}
int SaleableItem::getValue() {
	return value;
}
string SaleableItem::getStats() {
	return stats;
}
int SaleableItem::getID() {
	return ID;
}
int SaleableItem::getDamage() {
	return 0;
}
int SaleableItem:: getDefense() {
	return 0;
}
int SaleableItem:: getExtraHealth() {
	return 0;
}