#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#ifndef FANTASYSTORE_SALEABLEITEM_H
#define FANTASYSTORE_SALEABLEITEM_H


using namespace std;

class SaleableItem {
protected:
	char type;
	int value;
	string name;
	int ID;
	string stats;
public:
	SaleableItem();
	void setName(string name);
	void setValue(int value);
	void setID(int id);
	void setStats(string stat);
	void setType(char x);
	char getType();
	string getName();
	int getValue();
	string getStats();
	int getID();
	virtual int getDamage();
	virtual int getDefense();
	virtual int getExtraHealth();
};

#endif //FANTASYSTORE_SALEABLEITEM_H