//store cpp
#include "Shop.h"
#include <fstream>
#include <sstream>

using namespace std;

Shop::Shop() : Inventory() {
	if (cart.empty() == true) {
		int y = generateStore();
		if (y == -1) {
			cout << "One of the data files could not be read" << endl;
			cout << "Please fix this and relaunch (:" << endl;
		}
		else {
		}
	}
}
	void Shop ::clearCart() {
		cart.clear();
	}
	void Shop:: printShop() {//prints the shop
		cout << "*************SHOP*************" << endl;
		for (size_t i = 0; i < stock.size(); i++) {
			cout << " "<< i << " - " << stock[i]->getName() << " | " << stock[i]->getStats() << " | " << stock[i]->getValue() << " gold " << endl;
		}
		cout << "******************************" << endl << endl;
	}
	void Shop::printCart() {//prints the cart
		cout << "*************CART*************" << endl;
		for (size_t i = 0; i < cart.size(); i++) {
			cout << " " << i << " - " << cart[i]->getName() << " | " << cart[i]->getStats() << " | " << cart[i]->getValue() << " gold"  << endl;
		}
		cout << "******************************" << endl << endl;
	}
	/*int Shop::searchStock(string item) { //takes in the Name of the item and returns the spot in the shop///	
		for (size_t i = 0; i < stock.size(); i++) {
			if (stock[i].getName().compare(item) == 0) {
				return i;
			}
		}
		return -1;

	}
	int Shop::searchCart(string item) { //takes in the Name of the items and returns the spot in the cart				
		for (size_t i = 0; i < cart.size(); i++) {
			if (cart[i]->getName().compare(item) == 0) {
				return i;
			}
		}
		return -1;

	}*/
	void Shop::addToCart(int x) {//adds the spot in the stock, to the cart
		SaleableItem *gift = stock[x];
		cart.push_back(gift);
	}
	void Shop::removeFromCart(int x) {
		cart.erase(cart.begin() + x);
	}

	int Shop::generateStore() { //loads the store from the file

		ifstream infile1;
		infile1.open("Weapons.txt");

		if (!infile1.is_open() && stock.empty()==true)
		{
			cout << "Weapon.txt could not be opened" << endl;
			return -1;
		}
		string line1, dataString1;
		string substr1;
		Weapon* aWeapon;
			while (getline(infile1, line1))
			{
				aWeapon = new Weapon();
				istringstream dataStream(line1);
				getline(dataStream, substr1, '|'); //name
				aWeapon->setName(substr1);
				getline(dataStream, substr1, '|');//value
				aWeapon->setValue(stoi(substr1));
				getline(dataStream, substr1, '|');//ID
				aWeapon->setID(stoi(substr1));
				getline(dataStream, substr1, '|');//Stats
				aWeapon->setStats(substr1);
				getline(dataStream, substr1, '|');//damage
				aWeapon->setDamage(stoi(substr1));
				getline(dataStream, substr1, '|');
				aWeapon->setType(substr1[0]);//type (w)
				stock.push_back(aWeapon);
			}
		infile1.close();
		ifstream infile2;
		infile2.open("Armour.txt");
		if(!infile2.is_open())
		{
			cout << "Armour.txt could not be opened" << endl;
			return -1;
		}
		string line2, dataString2;
		string substr2;
		Armour *aArmour;

		while (getline(infile2, line2))
		{

			istringstream dataStream(line2);
			aArmour = new Armour();
			getline(dataStream, substr2, '|');
			aArmour->setName(substr2);
			getline(dataStream, substr2, '|');
			aArmour->setValue(stoi(substr2));
			getline(dataStream, substr2, '|');
			aArmour->setID(stoi(substr2));
			getline(dataStream, substr2, '|');
			aArmour->setStats(substr2);
			getline(dataStream, substr2, '|');
			aArmour->setDefense(stoi(substr2));
			getline(dataStream, substr1, '|');
			aArmour->setType(substr1[0]);
			stock.push_back(aArmour);
		}
		infile2.close();

		ifstream infile3;
		infile3.open("Item.txt");
		if(!infile3.is_open())
		{
			cout << "Item.txt could not be opened" << endl;
			return -1;
		}
		string line3, dataString3;
		string substr3;
		Item *aItem;

		while (getline(infile3, line3))
		{
			aItem = new Item();
			istringstream dataStream(line3);
			getline(dataStream, substr3, '|');
			aItem->setName(substr3);
			getline(dataStream, substr3, '|');
			aItem->setValue(stoi(substr3));
			getline(dataStream, substr3, '|');
			aItem->setID(stoi(substr3));
			getline(dataStream, substr3, '|');
			aItem->setStats(substr3);
			getline(dataStream, substr3, '|');
			aItem->setExtraHealth(stoi(substr3));
			getline(dataStream, substr1, '|');
			aItem->setType(substr1[0]);
			stock.push_back(aItem);
		}
		infile3.close();
		return 0;
			}	
		
	

	int Shop::getCartCost() {//returns the gold cost of the cart
		int money = 0;
		for (size_t i = 0; i < cart.size(); i++) {
			money = money + cart[i]->getValue();
		}
		return money;
	}
	SaleableItem* Shop::sendCartItem(int x){//returns an item from the cart
		SaleableItem *item;			
		item= cart[x];
		return item;

	}

	void Shop::addItemsToCart() {
		size_t x;
		int q = 0;
		do {     
			if (q == 0) {
					cout << endl;
					printShop();
					cout << "ShopKeeper: Which item would you like to add to your cart?" << endl;
					cin >> x;
					while (x < 0 || x >= stock.size()) {
						cout << "ShopKeeper: I'm sorry, but I didnt quite understand you." << endl;
						cout << "ShopKeeper: Which item would you like to add to your cart?" << endl;
						cin >> x;
					}
				addToCart(x);
				printCart();
				cout << "ShopKeeper: Would you like to add anything else to your cart?" << endl;
				cout << "[0] Yes" << endl;
				cout << "[1] No" << endl;
				cin >> q;
			}
			if (q != 0 && q != 1) {
				cout << "ShopKeeper: I'm sorry but I didnt understand that!" << endl;
				cout << "ShopKeeper: Would you like to add anything else to your cart? (yes/no)" << endl;
				cin >> q;
			}
		}

		while (q != 1);
	}
	

	void Shop::askToRemove() {
		int h=0;
		string m;
		size_t x;
		do {
			if (h == 0) {
				cout << "ShopKeeper: Which item would you like to remove from your cart?" << endl;
				printCart();
				cin >> x;
				while (x < 0 || x >= cart.size()) {
					cout << "ShopKeeper: I'm sorry, but I didnt quite understand you." << endl;
					cout << "ShopKeeper: Which item would you like to remove from your cart? (Please one item)" << endl;
					cin >> x;
				}
				cout << endl << endl;
				cout << "ShopKeeper: I removed the " << cart[x]->getName() << " from the cart for you." << endl;
				removeFromCart(x);
				printCart();
				if (cart.empty() == true) {
					cout << "ShopKeeper: Your cart is now empty." << endl << endl;
					h = 1;
				}
				else{
					cout << "Anything else?" << endl;
					cout << "[0] Yes" << endl;
					cout << "[1] No" << endl;
					cin >> h;
				}
			}
			if (h != 1 && h != 0) {
				cout << "ShopKeeper: Im sorry what was that?" << endl;
				cin >> h;
			}

		} while (h != 1 && cart.empty()==true);

	}

	int Shop::checkOut() {
		if (cart.empty() == true) {
			cout << "Your cart is empty!" << endl;
			return 0;
		}
		else {
			cout << "///////////////////////////" << endl;
			cout << "ShopKeeper: This is what I have you down for:" << endl;
			printCart();
			int cost = getCartCost();
			cout << "ShopKeeper: The cost of the cart is " << cost << " gold!" << endl;
			return cost;
		}
	}

	SaleableItem* Shop::idChecker(int x) {////change variable names
		for (int i = 0; i < stock.size(); i++) {
			if (stock[i]->getID() == x) {
				SaleableItem *gift = stock[x];
				return gift;
			}
		}
	}
