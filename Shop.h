#ifndef FANTASYSTORE_STORE
#define FANTASYSTORE_STORE
#include <iostream>
#include "Inventory.h"
#include "SaleableItem.h"


class Shop : protected Inventory {
protected:
	vector<SaleableItem*> stock;
	vector<SaleableItem> specialItems;
public:
	vector<SaleableItem*> cart;
	Shop();

	void clearCart();

	void printShop();

	void printCart();

	/*int searchStock(string item);

	int searchCart(string item);*/

	void addToCart(int x);
	
	void removeFromCart(int x);

	int generateStore();

	int getCartCost();

	void addItemsToCart();

	void askToRemove();

	int checkOut();

	SaleableItem* sendCartItem(int x);

	SaleableItem* idChecker(int x);
};


#endif // !FANTASYSTORE_STORE