#ifndef FANTASYSTORE_WEAPON
#define FANTASYSTORE_WEAPON
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include "SaleableItem.h"

class Weapon : public SaleableItem {
	int damage;
public:
	Weapon();
	int getDamage();
	void setDamage(int x);
};

#endif // !FANTASYSTORE_WEAPON
